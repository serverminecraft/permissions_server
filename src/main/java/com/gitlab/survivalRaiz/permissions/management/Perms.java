package com.gitlab.survivalRaiz.permissions.management;

import org.bukkit.entity.Player;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Defines all permissions for the server.
 * This ensures no typo can be made in the code, for permissions at least.
 */
public enum Perms {
    SR("sr"),
        CLANS(SR, "clans"),
            DEL_ANY_CLAN(CLANS, "delany"),
        ECONOMY(SR, "economy"),
            SUPER_WALLET(ECONOMY, "super-wallet"),
            TAKE_MONEY(ECONOMY, "take-money"),
            BANKER(ECONOMY, "banker"),
            ADMIN_SHOP(ECONOMY, "admin-shop"),
                CREATE_SHOP(ADMIN_SHOP, "create-shop"),
        LAND_MANAGER(SR, "landmanager"),
            GIVE_ALL_TERRAINS(LAND_MANAGER, "give-all-terrains"),
            PAY_OTHERS_TAXES(LAND_MANAGER, "pay-others-taxes"),
    // TODO: permissions for kits and chat plugins are managed from the server software, consider moving here to unify all
//        KITS(SR, "kits"),
//            GET_KIT(KITS, "get")
    ;


    /**
     * The permission node containing the permission string, like "sr.chat"
     */
    private final PermNode node;

    Perms(String perm) {
        this.node = new PermNode(perm);
    }

    Perms(Perms parent, String perm) {
        this.node = new PermNode(perm, parent.node);
    }

    /**
     * Generates a new permission, useful for controlling access to a feature that can be extended at runtime and therefore requires new permissions to be created at runtime.
     * This system is implemented in the chat system, since new channels can be created at any time.
     * @param appendix the permission suffix.
     * @return A permission object that can validate whether a player has it or not.
     */
    public PermNode append(String appendix) {
        return new PermNode(appendix, this.node);
    }

    /**
     * Checks if the player has a certain permission.
     * Runs {@link PermNode#allows(Player)} for a not so verbose check, otherwise a getter for the {@link PermNode} of this Perm would be needed
     * @param p the player to be checked
     * @return true if the player has the permission
     */
    public boolean allows(Player p) {
        return node.allows(p);
    }

    /**
     * @return the permission value
     */
    public String getValue() {
        return node.getValue();
    }

    /**
     * Extracts all perms of all plugins to a Set of Strings
     * @return all permissions as a String Set
     */
    public static Set<String> getAllPerms() {
        final Set<String> perms = new HashSet<>();

        for (Perms p : values())
            perms.addAll(Arrays.asList(p.getValue().split(".")));

        return perms;
    }
}
